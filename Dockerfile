FROM ubuntu:20.04

ENV LITECOIN_VERSION=0.18.1
ENV KEYSERVER=keyserver.ubuntu.com
ENV KEY=FE3348877809386C

# Create user litecoin
RUN useradd -r litecoin -s /bin/sh -m -u 1001\
# Update packages
  && apt-get update -y \
# Install curl package
  && apt-get install -y curl gnupg \
# Clean apt cache
  && apt-get clean

# Download litecoin & litecoin gpg key
RUN curl -SLO https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz \
 && curl -SLO https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-linux-signatures.asc \
# Get pgp public key & fingerprint
 && gpg --keyserver ${KEYSERVER} --recv-key ${KEY} \
 && gpg --fingerprint ${KEY} \
# Verify gpg key of download release
 && gpg --verify litecoin-${LITECOIN_VERSION}-linux-signatures.asc \
 && grep $(sha256sum litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz | awk '{print $1}') litecoin-0.18.1-linux-signatures.asc \
 && rm -f litecoin-${LITECOIN_VERSION}-linux-signatures.asc

# Uncompress tar file to /usr/local/bin
# Copy litecoin binary to local binary directory
# Delete tar.gz file
RUN tar -xzf litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz -C /tmp \
  && mv /tmp/litecoin-${LITECOIN_VERSION}/bin/* /usr/local/bin/ \
  && rm -f litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz && rm -rf /tmp/

# Create litecoin user
USER litecoin
WORKDIR /home/litecoin
COPY litecoin.conf $HOME/.litecoin/litecoin.conf
ENV PATH "$PATH:/usr/local/bin"
ENV LITECOIN_DATA=$HOME/.litecoin/data
VOLUME $HOME/.litecoin/data

# Expose ports: Mainnet REST 9332 Mainnet P2P 9333 Testnet JSON 19332 Testnet P2P 19333 Regtest REST 19332 Regtest P2P 19444
EXPOSE 9332 9333 19332 19333 19444

CMD [ "sh" ]