# devops-test

1. Docker-ayes: Write a Dockerfile to run Litecoin 0.18.1 in a container. It should somehow verify the
checksum of the downloaded release (there's no need to build the project), run as a normal user, and
when run without any modifiers (i.e. docker run somerepo/litecoin:0.18.1) should run the daemon,
and print its output to the console. The build should be security conscious (and ideally pass a
container image security test such as Anchore).
2. k8s FTW: Write a Kubernetes StatefulSet to run the above, using persistent volume claims
and resource limits.
3. All the continuouses: Write a simple build and deployment pipeline for the above using groovy
/ Jenkinsfile, Travis CI or Gitlab CI.
---
4. Script kiddies: Source or come up with a text manipulation problem and solve it with at least two of awk, sed, tr and / or grep. Check the question below first though, maybe.
5. Script grown-ups: Solve the problem in question 4 using any programming language you like.
6. Terraform lovers unite: write a Terraform module that creates the following resources in IAM; ---

- A role, with no permissions, which can be assumed by users within the same account,
• - A policy, allowing users / entities to assume the above role,
• - A group, with the above policy attached,
• - A user, belonging to the above group.

## Tech stacks
Litecoin
Gitlab CI
Linux
k8s
Trivy
Docker
Terraform

## References
Bitcoin docker
https://hub.docker.com/r/bitcoinsv/bitcoin-sv

Trivy container Scanner
https://github.com/aquasecurity/trivy

GPG key check for litecoin
https://download.litecoin.org/README-HOWTO-GPG-VERIFY-TEAM-MEMBERS-KEY.txt

litecoin github
https://github.com/litecoin-project/litecoin